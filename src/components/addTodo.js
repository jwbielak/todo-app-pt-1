import React from "react";


export default class TodoForm extends React.Component {
  state = {
    text: ""
  };

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleSubmit = event => {
    
    this.props.onSubmit({
      id: 0,
      title: this.state.text,
      complete: false
    });
    this.setState({
      text: ""
    });
  };

  render() {
    return (
       <input className="new-todo" placeholder="What needs to be done?" autoFocus 
          name="text"
          value={this.state.text}
          onChange={event => {this.setState({text: event.target.value})}}
                 onKeyPress={event => {
                  
                if (event.key === 'Enter') {
                   this.handleSubmit()
                }
              }}

         />
     
    );
  }
}
        
        
        
        
        
        
 