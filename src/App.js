import React, { Component } from "react";
import todosList from "./todos.json";
import AddTodo from './components/addTodo'


class App extends Component {
  state = {
    todos: todosList,
  };

  removeAllTodosThatAreComplete = () => {
   
    this.setState(state => ({
      todos: state.todos.filter(todo => todo.completed !== true)
    }));
  }

  handleDeleteTodo = id => {

    this.setState(state => ({
      todos: state.todos.filter(todo => todo.id !== id)
   }));
    
  };

  toggleComplete = id => {
   
       let newTodos = this.state.todos.map(todo => { 
        if (todo.id === id) {      
          
            todo.completed = !todo.completed
        
        } return todo
      })
  
      this.setState({todos:newTodos})

  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
      
          <AddTodo onSubmit = { (todo) => this.setState({todos: [...this.state.todos, todo]})} />
         
        </header>
        <TodoList todos={this.state.todos} handleDeleteTodo={this.handleDeleteTodo} toggleComplete={this.toggleComplete}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
       
        <button className ='clear-completed'
        onClick={() => this.removeAllTodosThatAreComplete()}
         >Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li
      className={this.props.completed ? "completed" : "" }>
          <div className="view">
          <input className="toggle" type="checkbox" onChange={ () =>
             this.props.toggleComplete(this.props.id)}
              checked={this.props.completed} />
          <label>{this.props.title}</label> 
          <button onClick={ () => this.props.handleDeleteTodo(this.props.id)} className = 'destroy'/>
         </div>
      </li>
    );
  }
}



class TodoList extends Component {
  render() {
    return (
      <section className='main'>
        <ul className='todo-list'>
          {this.props.todos.map((todo, i) => (
            <TodoItem title={todo.title} key={i} id={todo.id} 
            completed={todo.completed}  handleDeleteTodo={this.props.handleDeleteTodo} toggleComplete={this.props.toggleComplete} />
          ))}
        </ul>
      </section>
    );
  }
}




export default App;




export default App;
